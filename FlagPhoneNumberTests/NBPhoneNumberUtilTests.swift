//
//  FlagPhoneNumberTests.swift
//  FlagPhoneNumberTests
//
//  Created by Carlos Monzon on 30/4/21.
//  Copyright © 2021 chronotruck. All rights reserved.
//

import XCTest
@testable import FlagPhoneNumber

class NBPhoneNumberUtilTests: XCTestCase {
    
    let phoneUtil = NBPhoneNumberUtil()

    func testValidAustralianPhoneNumber() throws {
        XCTAssertTrue(phoneUtil.isValidNumber(try! phoneUtil.parse("+610451111222", defaultRegion: "AU")))
        XCTAssertTrue(phoneUtil.isValidNumber(try! phoneUtil.parse("+61451111222", defaultRegion: "AU")))
        XCTAssertTrue(phoneUtil.isValidNumber(try! phoneUtil.parse("+61483111222", defaultRegion: "AU")))
        XCTAssertTrue(phoneUtil.isValidNumber(try! phoneUtil.parse("+610483111222", defaultRegion: "AU")))
    }

}

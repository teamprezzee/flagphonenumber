import UIKit

public struct FPNCountry: Equatable {

	public var code: FPNCountryCode
    private var codeStr: String
	public var name: String
	public var phoneCode: String
    var flag: UIImage? {
        if let flag = UIImage(named: self.codeStr, in: Bundle.FlagIcons, compatibleWith: nil) {
            return flag
        } else {
            return UIImage(named: "unknown", in: Bundle.FlagIcons, compatibleWith: nil)
        }
    }

	init(code: String, name: String, phoneCode: String) {
		self.name = name
		self.phoneCode = phoneCode
		self.code = FPNCountryCode(rawValue: code)!
        self.codeStr = code
	}

	static public func ==(lhs: FPNCountry, rhs: FPNCountry) -> Bool {
		return lhs.code == rhs.code
	}
}

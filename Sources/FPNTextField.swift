//
//  FlagPhoneNumberTextField.swift
//  FlagPhoneNumber
//
//  Created by Aurélien Grifasi on 06/08/2017.
//  Copyright (c) 2017 Aurélien Grifasi. All rights reserved.
//

import UIKit

open class FPNTextField: UITextField, UIGestureRecognizerDelegate {

	/// The size of the flag button
	@objc open var flagButtonSize: CGSize = CGSize(width: 32, height: 32) {
		didSet {
			layoutIfNeeded()
		}
	}
    
    public var setupBorders: Bool = true {
        didSet {
            if setupBorders {
                layer.addSublayer(self.borderLayer)
            } else {
                self.borderLayer.removeFromSuperlayer()
            }
        }
    }
    
    private lazy var borderLayer: CALayer = {
        let borderWidth:CGFloat = 2.0
        let borderLayer = CALayer()
        borderLayer.borderColor = UIColor(red: 193.0/255.0, green: 193.0/255.0, blue: 193.0/255.0, alpha: 1.0).cgColor
        borderLayer.frame = CGRect(x: 0, y: self.frame.size.height - borderWidth, width: self.frame.size.width + 10, height: 1)
        borderLayer.borderWidth =  borderWidth
        return borderLayer
    }()

	private var flagWidthConstraint: NSLayoutConstraint?
	private var flagHeightConstraint: NSLayoutConstraint?

    private var iconSize: CGSize = CGSize(width:5, height:5)

    private lazy var tapRecogniser: UITapGestureRecognizer = {
        let recognizer = UITapGestureRecognizer(target: self, action:#selector(displayCountries))
        recognizer.delegate = self

        return recognizer
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView()
        if let img = UIImage(named: "DownArrow", in: Bundle.FlagIcons, compatibleWith: nil) {
            imageView.image = img
        }

        imageView.frame = CGRect(x: 0, y: 0, width: iconSize.width, height: iconSize.height)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.setContentHuggingPriority(UILayoutPriority.defaultHigh, for: .horizontal)
        imageView.setContentCompressionResistancePriority(UILayoutPriority.defaultHigh, for: .horizontal)
        imageView.contentMode = .scaleAspectFit
        imageView.isUserInteractionEnabled = true

        return imageView
    }()

	/// The size of the leftView
	private var leftViewSize: CGSize {
        let width = flagButtonSize.width + iconSize.width + getWidth(text: phoneCodeTextField.text!) + 10
		let height = bounds.height

		return CGSize(width: width, height: height)
	}

	private var phoneCodeTextField: UITextField = UITextField()

	private lazy var phoneUtil: NBPhoneNumberUtil = NBPhoneNumberUtil()
	private var nbPhoneNumber: NBPhoneNumber?
	private var formatter: NBAsYouTypeFormatter?

	open var flagButton: UIButton = UIButton()

	open override var font: UIFont? {
		didSet {
			phoneCodeTextField.font = font
		}
	}

	open override var textColor: UIColor? {
		didSet {
			phoneCodeTextField.textColor = textColor
		}
	}

	/// Present in the placeholder an example of a phone number according to the selected country code.
	/// If false, you can set your own placeholder. Set to true by default.
	@objc open var hasPhoneNumberExample: Bool = true {
		didSet {
			if hasPhoneNumberExample == false {
				placeholder = nil
			}
			updatePlaceholder()
		}
	}

	open var countryRepository = FPNCountryRepository()

	open var selectedCountry: FPNCountry? {
		didSet {
			updateUI()
		}
	}

	/// Input Accessory View for the texfield
	@objc open var textFieldInputAccessoryView: UIView?
    
    open var showPhoneNumbers: Bool = true {
        didSet {
            pickerView.showPhoneNumbers = showPhoneNumbers
        }
    }
    
    open var uiMode: FPNUiMode = .phone {
        didSet {
            switch uiMode {
            case .country:
                pickerView.showPhoneNumbers = false
            default:
                pickerView.showPhoneNumbers = true
            }
            updateUI()
            updateTargets()
            updateHelperView(for: uiMode)
        }
    }

	open lazy var pickerView: FPNCountryPicker = FPNCountryPicker()

	@objc public enum FPNDisplayMode: Int {
		case picker
		case list
	}

	@objc open var displayMode: FPNDisplayMode = .picker

	init() {
		super.init(frame: .zero)

		setup()
	}

	public override init(frame: CGRect) {
		super.init(frame: frame)

		setup()
	}

	required public init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)

		setup()
	}
    
    private func updateTargets() {
        switch uiMode {
        case .country:
            removeTarget(nil, action: nil, for: .allEvents)
            setPickerInputView(shouldFocus: false)
        default:
            removeTarget(nil, action: nil, for: .allEvents)
            addTarget(self, action: #selector(displayNumberKeyBoard), for: .touchDown)
            addTarget(self, action: #selector(didEditText), for: .editingChanged)
        }
    }

	private func setup() {
		leftViewMode = .always

        textColor = UIColor(red: 71.0/255.0, green: 65.0/255.0, blue: 65.0/255.0, alpha: 1.0)
		setupFlagButton()
		setupPhoneCodeTextField()
		setupHelperView()

		keyboardType = .numberPad
		autocorrectionType = .no
        updateTargets()
		if let regionCode = Locale.current.regionCode, let countryCode = FPNCountryCode(rawValue: regionCode) {
			setFlag(countryCode: countryCode)
		} else {
			setFlag(countryCode: FPNCountryCode.FR)
		}
	}

	private func setupFlagButton() {
		flagButton.imageView?.contentMode = .scaleAspectFit
		flagButton.accessibilityLabel = "flagButton"
		flagButton.addTarget(self, action: #selector(displayCountries), for: .touchUpInside)
		flagButton.translatesAutoresizingMaskIntoConstraints = false
		flagButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 5)
	}

	private func setupPhoneCodeTextField() {
		phoneCodeTextField.font = font
        phoneCodeTextField.translatesAutoresizingMaskIntoConstraints = false
        phoneCodeTextField.delegate = self
	}

	private func setupHelperView() {
		leftView = UIView()

		leftViewMode = .always
		if #available(iOS 9.0, *) {
			phoneCodeTextField.semanticContentAttribute = .forceLeftToRight
		} else {
			// Fallback on earlier versions
		}
		leftView?.addSubview(flagButton)
		leftView?.addSubview(phoneCodeTextField)
       

		flagWidthConstraint = NSLayoutConstraint(item: flagButton, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: flagButtonSize.width)
		flagHeightConstraint = NSLayoutConstraint(item: flagButton, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 0, constant: flagButtonSize.height)

		flagWidthConstraint?.isActive = true
		flagHeightConstraint?.isActive = true

		NSLayoutConstraint(item: flagButton, attribute: .centerY, relatedBy: .equal, toItem: leftView, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
		NSLayoutConstraint(item: flagButton, attribute: .leading, relatedBy: .equal, toItem: leftView, attribute: .leading, multiplier: 1, constant: 0).isActive = true

        updateHelperView(for: uiMode)

        leftView?.isUserInteractionEnabled = true
        leftView?.addGestureRecognizer(tapRecogniser)
	}
    
    private func updateHelperView(for mode: FPNUiMode) {
        iconImageView.removeConstraints(iconImageView.constraints)
        phoneCodeTextField.removeConstraints(phoneCodeTextField.constraints)
        switch mode {
        case .country:
            rightViewMode = UITextField.ViewMode.always
            rightView = iconImageView
            rightView?.addGestureRecognizer(tapRecogniser)
            NSLayoutConstraint(item: phoneCodeTextField, attribute: .leading, relatedBy: .equal, toItem: flagButton, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
        
        default:
            leftView?.addSubview(iconImageView)
            NSLayoutConstraint(item: iconImageView, attribute: .centerY, relatedBy: .equal, toItem: leftView, attribute: .centerY, multiplier: 1, constant: 0).isActive = true
            NSLayoutConstraint(item: iconImageView, attribute: .leading, relatedBy: .equal, toItem: flagButton, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
            NSLayoutConstraint(item: phoneCodeTextField, attribute: .leading, relatedBy: .equal, toItem: iconImageView, attribute: .trailing, multiplier: 1, constant: 0).isActive = true
            NSLayoutConstraint(item: phoneCodeTextField, attribute: .trailing, relatedBy: .equal, toItem: leftView, attribute: .trailing, multiplier: 1, constant: -5).isActive = true
        }
        NSLayoutConstraint(item: phoneCodeTextField, attribute: .top, relatedBy: .equal, toItem: leftView, attribute: .top, multiplier: 1, constant: 0).isActive = true
        NSLayoutConstraint(item: phoneCodeTextField, attribute: .bottom, relatedBy: .equal, toItem: leftView, attribute: .bottom, multiplier: 1, constant: 0).isActive = true
    }

	open override func updateConstraints() {
		super.updateConstraints()

		flagWidthConstraint?.constant = flagButtonSize.width
		flagHeightConstraint?.constant = flagButtonSize.height
	}
    
    open func hideArrow() {
        self.iconImageView.isHidden = true
    }

	open override func leftViewRect(forBounds bounds: CGRect) -> CGRect {
		let size = leftViewSize
		let width: CGFloat = min(bounds.size.width, size.width)
		let height: CGFloat = min(bounds.size.height, size.height)
		let newRect: CGRect = CGRect(x: bounds.minX, y: bounds.minY, width: width, height: height)

		return newRect
	}

	@objc private func displayNumberKeyBoard() {
		switch displayMode {
		case .picker:
			tintColor = .gray
			inputView = nil
			inputAccessoryView = textFieldInputAccessoryView
			reloadInputViews()
		default:
			break
		}
	}
    
    private func setPickerInputView(shouldFocus: Bool) {
        pickerView.setup(repository: countryRepository)

        tintColor = .clear
        inputView = pickerView
        inputAccessoryView = getToolBar(with: getCountryListBarButtonItems())
        reloadInputViews()
        if shouldFocus {
            becomeFirstResponder()
        }

        pickerView.didSelect = { [weak self] country in
            self?.fpnDidSelect(country: country)
        }

        if let selectedCountry = selectedCountry {
            pickerView.setCountry(selectedCountry.code)
        } else if let regionCode = Locale.current.regionCode, let countryCode = FPNCountryCode(rawValue: regionCode) {
            pickerView.setCountry(countryCode)
        } else if let firstCountry = countryRepository.countries.first {
            pickerView.setCountry(firstCountry.code)
        }
    }

	@objc private func displayCountries() {
		switch displayMode {
		case .picker:
            setPickerInputView(shouldFocus: true)
		case .list:
			(delegate as? FPNTextFieldDelegate)?.fpnDisplayCountryList()
		}
	}

	@objc private func dismissCountries() {
		resignFirstResponder()
        endEditing(true)
        if uiMode == .phone {
            inputView = nil
            inputAccessoryView = nil
            reloadInputViews()
        }
	}

	private func fpnDidSelect(country: FPNCountry) {
		(delegate as? FPNTextFieldDelegate)?.fpnDidSelectCountry(name: country.name, dialCode: country.phoneCode, code: country.code.rawValue)
		selectedCountry = country
	}

	// - Public

	/// Get the current formatted phone number
	open func getFormattedPhoneNumber(format: FPNFormat) -> String? {
		return try? phoneUtil.format(nbPhoneNumber, numberFormat: convert(format: format))
	}

	/// For Objective-C, Get the current formatted phone number
	@objc open func getFormattedPhoneNumber(format: Int) -> String? {
		if let formatCase = FPNFormat(rawValue: format) {
			return try? phoneUtil.format(nbPhoneNumber, numberFormat: convert(format: formatCase))
		}
		return nil
	}

	/// Get the current raw phone number
	@objc open func getRawPhoneNumber() -> String? {
		let phoneNumber = getFormattedPhoneNumber(format: .E164)
		var nationalNumber: NSString?

		phoneUtil.extractCountryCode(phoneNumber, nationalNumber: &nationalNumber)

		return nationalNumber as String?
	}

    /// Get whether the entered phone code / phone number combination is valid
    @objc public func isValid() -> Bool {
        guard let phoneCode = selectedCountry?.phoneCode, let number = text else {
            return false
        }

        let cleanedPhoneNumber = clean(string: "\(phoneCode) \(number)")

        guard let _ = getValidNumber(phoneNumber: cleanedPhoneNumber) else {
            return false
        }

        return true
    }

	/// Set directly the phone number. e.g "+33612345678"
	@objc open func set(phoneNumber: String) {
		let cleanedPhoneNumber: String = clean(string: phoneNumber)

		if let validPhoneNumber = getValidNumber(phoneNumber: cleanedPhoneNumber) {
			if validPhoneNumber.italianLeadingZero {
				text = "0\(validPhoneNumber.nationalNumber.stringValue)"
			} else {
				text = validPhoneNumber.nationalNumber.stringValue
			}
			setFlag(countryCode: FPNCountryCode(rawValue: phoneUtil.getRegionCode(for: validPhoneNumber))!)
		}
	}

	/// Set the country image according to country code. Example "FR"
	open func setFlag(countryCode: FPNCountryCode) {
		let countries = countryRepository.countries

		for country in countries {
			if country.code == countryCode {
				return fpnDidSelect(country: country)
			}
		}
	}

	/// Set the country image according to country code. Example "FR"
	@objc open func setFlag(key: FPNOBJCCountryKey) {
		if let code = FPNOBJCCountryCode[key], let countryCode = FPNCountryCode(rawValue: code) {

			setFlag(countryCode: countryCode)
		}
	}

	/// Set the country list excluding the provided countries
	open func setCountries(excluding countries: [FPNCountryCode]) {
		countryRepository.setup(without: countries)

		if let selectedCountry = selectedCountry, countryRepository.countries.contains(selectedCountry) {
			fpnDidSelect(country: selectedCountry)
		} else if let country = countryRepository.countries.first {
			fpnDidSelect(country: country)
		}
	}

	/// Set the country list including the provided countries
	open func setCountries(including countries: [FPNCountryCode]) {
		countryRepository.setup(with: countries)

		if let selectedCountry = selectedCountry, countryRepository.countries.contains(selectedCountry) {
			fpnDidSelect(country: selectedCountry)
		} else if let country = countryRepository.countries.first {
			fpnDidSelect(country: country)
		}
	}

	/// Set the country list excluding the provided countries
	@objc open func setCountries(excluding countries: [Int]) {
		let countryCodes: [FPNCountryCode] = countries.compactMap({ index in
			if let key = FPNOBJCCountryKey(rawValue: index), let code = FPNOBJCCountryCode[key], let countryCode = FPNCountryCode(rawValue: code) {
				return countryCode
			}
			return nil
		})

		countryRepository.setup(without: countryCodes)
	}

	/// Set the country list including the provided countries
	@objc open func setCountries(including countries: [Int]) {
		let countryCodes: [FPNCountryCode] = countries.compactMap({ index in
			if let key = FPNOBJCCountryKey(rawValue: index), let code = FPNOBJCCountryCode[key], let countryCode = FPNCountryCode(rawValue: code) {
				return countryCode
			}
			return nil
		})

		countryRepository.setup(with: countryCodes)
	}

	// Private

	@objc private func didEditText() {
		if let phoneCode = selectedCountry?.phoneCode, let number = text {
			var cleanedPhoneNumber = clean(string: "\(phoneCode) \(number)")

			if let validPhoneNumber = getValidNumber(phoneNumber: cleanedPhoneNumber) {
				nbPhoneNumber = validPhoneNumber

				cleanedPhoneNumber = "+\(validPhoneNumber.countryCode.stringValue)\(validPhoneNumber.nationalNumber.stringValue)"

				if let inputString = formatter?.inputString(cleanedPhoneNumber) {
					text = remove(dialCode: phoneCode, in: inputString)
				}
				(delegate as? FPNTextFieldDelegate)?.fpnDidValidatePhoneNumber(textField: self, isValid: true)
			} else if let validPhoneNumber = getValidNumber(phoneNumber: clean(string: number)) {
                //To handle copy paste number with country code.
                if validPhoneNumber.italianLeadingZero {
                    text = "0\(validPhoneNumber.nationalNumber.stringValue)"
                } else {
                    text = validPhoneNumber.nationalNumber.stringValue
                }
                setFlag(countryCode: FPNCountryCode(rawValue: phoneUtil.getRegionCode(for: validPhoneNumber))!)
            } else {
                nbPhoneNumber = nil
				if let dialCode = selectedCountry?.phoneCode {
					if let inputString = formatter?.inputString(cleanedPhoneNumber) {
						text = remove(dialCode: dialCode, in: inputString)
					}
				}
				(delegate as? FPNTextFieldDelegate)?.fpnDidValidatePhoneNumber(textField: self, isValid: false)
			}
		}
	}

	private func convert(format: FPNFormat) -> NBEPhoneNumberFormat {
		switch format {
		case .E164:
			return NBEPhoneNumberFormat.E164
		case .International:
			return NBEPhoneNumberFormat.INTERNATIONAL
		case .National:
			return NBEPhoneNumberFormat.NATIONAL
		case .RFC3966:
			return NBEPhoneNumberFormat.RFC3966
		}
	}

	private func updateUI() {
		if let countryCode = selectedCountry?.code {
			formatter = NBAsYouTypeFormatter(regionCode: countryCode.rawValue)
		}

		flagButton.setImage(selectedCountry?.flag, for: .normal)

        if let phoneCode = selectedCountry?.phoneCode {
            phoneCodeTextField.text = phoneCode
        }
        
        updatePhoneCodeTextField()
        
		if hasPhoneNumberExample == true {
			updatePlaceholder()
		}
		didEditText()
	}
    
    private func updatePhoneCodeTextField() {
        guard let selectedCountry = selectedCountry else { return }
        switch uiMode {
        case .country:
            phoneCodeTextField.text = selectedCountry.name
        default:
            phoneCodeTextField.text = selectedCountry.phoneCode
        }
    }

	private func clean(string: String) -> String {
		var allowedCharactersSet = CharacterSet.decimalDigits

		allowedCharactersSet.insert("+")

		return string.components(separatedBy: allowedCharactersSet.inverted).joined(separator: "")
	}

	private func getWidth(text: String) -> CGFloat {
		if let font = phoneCodeTextField.font {
			let fontAttributes = [NSAttributedString.Key.font: font]
			let size = (text as NSString).size(withAttributes: fontAttributes)

			return size.width.rounded(.up)
		} else {
			phoneCodeTextField.sizeToFit()

			return phoneCodeTextField.frame.size.width.rounded(.up)
		}
	}

	private func getValidNumber(phoneNumber: String) -> NBPhoneNumber? {
		guard let countryCode = selectedCountry?.code else { return nil }

		do {
			let parsedPhoneNumber: NBPhoneNumber = try phoneUtil.parse(phoneNumber, defaultRegion: countryCode.rawValue)
			let isValid = phoneUtil.isValidNumber(parsedPhoneNumber)

			return isValid ? parsedPhoneNumber : nil
		} catch _ {
			return nil
		}
	}

	private func remove(dialCode: String, in phoneNumber: String) -> String {
		return phoneNumber.replacingOccurrences(of: "\(dialCode) ", with: "").replacingOccurrences(of: "\(dialCode)", with: "")
	}

	private func getToolBar(with items: [UIBarButtonItem]) -> UIToolbar {
		let toolbar: UIToolbar = UIToolbar()

		toolbar.barStyle = UIBarStyle.default
		toolbar.items = items
		toolbar.sizeToFit()

		return toolbar
	}

	private func getCountryListBarButtonItems() -> [UIBarButtonItem] {
		let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
		let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(dismissCountries))

		doneButton.accessibilityLabel = "doneButton"

		return [space, doneButton]
	}

	private func updatePlaceholder() {
        if uiMode == .country {
            placeholder = nil
            return
        }
		if let countryCode = selectedCountry?.code {
			do {
                let example = try phoneUtil.getExampleNumber(forType: countryCode.rawValue, type: NBEPhoneNumberType.MOBILE)
				let phoneNumber = "+\(example.countryCode.stringValue)\(example.nationalNumber.stringValue)"

				if let inputString = formatter?.inputString(phoneNumber) {
					placeholder = remove(dialCode: "+\(example.countryCode.stringValue)", in: inputString)
				} else {
					placeholder = nil
				}
			} catch _ {
				placeholder = nil
			}
		} else {
			placeholder = nil
		}
	}
}

// MARK: - UITextFieldDelegate

extension FPNTextField: UITextFieldDelegate {

    public func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {

        // Prevent editing phone code text field
        if textField == phoneCodeTextField {
            displayCountries()
            return false
        }

        return true

    }

}

public enum FPNUiMode {
    case phone
    case country
}
